import XCTest

extension MoviedbKit_CoreUnitTests {
    static let __allTests = [
        ("testItShouldAtLeastCompile", testItShouldAtLeastCompile)
    ]
}

#if !os(macOS)
public func __allTests() -> [XCTestCaseEntry] {
    return [
        testCase(MoviedbKit_CoreUnitTests.__allTests)
    ]
}
#endif
