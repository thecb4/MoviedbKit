import XCTest

import MoviedbKitUnitTests

var tests = [XCTestCaseEntry]()
tests += MoviedbKitUnitTests.__allTests()

XCTMain(tests)
