import Foundation
import Danger

let allSourceFiles = danger.git.modifiedFiles + danger.git.createdFiles

let changelogChanged = allSourceFiles.contains("CHANGELOG.md")
let sourceChanges = allSourceFiles.first(where: { $0.hasPrefix("app") })

if !changelogChanged && sourceChanges != nil {
  warn("No CHANGELOG entry added.")
}