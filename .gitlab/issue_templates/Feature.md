# { Feature Name }

As { Role }

I want { Goal }

So that { Benefit }

<br/>

# Scenarios

## Scenario 1
  
  Given { State }

  When { Behavior }

  Then { Outcome }

## Scenario 2
  
  Given { State }

  When { Behavior }

  THen { Outcome }