import Foundation
import HyperSpace

struct MoviedbEndPoint: EndpointType {

  enum Route: RouteType {
    func mockHTTPResponse(url: URL, statusCode: HTTPStatusCode) -> HTTPURLResponse? {
      return nil
    }

    // get list of groups for app in team
    // /discover/movie?primary_release_date.gte=2014-09-15&primary_release_date.lte=2014-10-22
    case discover(apiKey: String, since: String)

    var route: URL.Route {

      switch self {

        case .discover(let apiKey, let since):
          // "https://itunesconnect.apple.com/testflight/v2/providers/\(teamID)/apps/\(appID)/groups"
          return URL.Route(path: ["discover", "movie"])

      }

    }

    var method: URL.Method {
      switch self {
        case .discover:
          return .get
      }
    }

    var headers: [HTTPHeader] {
      switch self {
        case .discover:
          let headers =
            [
              HTTPHeader(field: "Content-Type", value: "application/json"),
              HTTPHeader(field: "Accept", value: "application/json, text/javascript")
             ]
          return headers
      }
    }

    var body: Data? {
      switch self {
        case .discover:
          return nil
      }
    }

    var mockResponseData: Data {
      switch self {
        case .discover:
          return "{\"status\": \"success\"}".data(using: String.Encoding.utf8)!
      }
    }

  }

  static let current = URL.Env(.https, "api.themoviedb.org").at("2")

}
