[//]: # "This actually is the most platform independent comment"

# MoviedbKit

MoviedbKit is a [HyperSpace](https://github.com/thecb4/HyperSpace) based service for accessing [TheMovieDB.org](https://www.themoviedb.org)

[![SPM](https://img.shields.io/badge/Swift_PM-compatible-brightgreen.svg?style=for-the-badge)](https://swift.org/package-manager)
![Linux](https://img.shields.io/badge/Platforms-macOS_Linux-blue.svg?style=for-the-badge)
[![Git Version](https://img.shields.io/gitlab/release/thecb4/MoviedbKit.svg?style=for-the-badge)](https://gitlab.com/thecb4/MoviedbKit/releases)
[![build status](https://gitlab.com/thecb4/MoviedbKit/badges/master/pipeline.svg)](https://gitlab.com/thecb4/MoviedbKit/commits/master)
[![license](https://img.shields.io/github/license/thecb4/MOviedbKit.svg?style=for-the-badge)](https://github.com/thecb4/MoviedbKit/blob/master/LICENSE)

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

1. Get an API key-> <a href="themoviedb.org">themoviedb.org</a>
<blockquote>
<p>"A TMDb user account is required to request an API key."*</p>
</blockquote>
2. View <a href="https://github.com/gkye/TheMovieDatabaseSwiftWrapper/wiki">Wikis</a> for examples 
<br>

### Installing

## Carthage

```
github "thecb4/MoviedbKit" ~> 0.1
```

## Package.swift

```
.package(url: "https://github.com/thecb4/MoviedbKit.git", .upToNextMinor(from: "0.1.0"))
```

End with an example of getting some data out of the system or using it for a little demo

## Running the tests

Explain how to run the automated tests for this system

### Break down into end to end tests

Explain what these tests test and why

```
Give an example
```

### And coding style tests

Explain what these tests test and why

```
Give an example
```

## Deployment

Add additional notes about how to deploy this on a live system

## Built With

* [HyperSpace](https://github.com/thecb4/HyperSpace) - Type Safe EndPoints
* [BrightFutures](https://github.com/thecb4/BrightFutures) - Asynchronous Behavior
* [Result](https://github.com/thecb4/Result) - Type Safe Responses

## Contributing

Please read [CONTRIBUTING.md](https://gist.github.com/PurpleBooth/b24679402957c63ec426) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags).

## Authors

* **Cavelle Benjamin** - *Initial work* - [thecb4](https://github.com/thecb4)

See also the list of [contributors](https://github.com/your/project/contributors) who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Hat tip to anyone who's code was used
* Inspiration
* etc